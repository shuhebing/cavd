# -*- coding: UTF-8 -*-
import os
import re
from cavd.netio import *
from monty.io import zopen
from cavd.channel import Channel
from cavd.area_volume import asa_new
from cavd.cavd_consts import LOWER_THRESHOLD, UPPER_THRESHOLD
from cavd.netstorage import AtomNetwork, connection_values_list
from cavd.recovery import rediscovery, rediscovery_kdTree, rediscovery_byRad_kdTree
from cavd.get_Symmetry import get_symnum_sites, get_equivalent_vornet,get_labeled_vornet
from cavd.local_environment import CifParser_new, LocalEnvirCom, get_local_envir_fromstru
from cavd.channel_analysis import MigrationPaths
from cavd.mergecluster import load_voids_channels_from_file
from cavd.mergecluster import MergeCluster

def cal_channel_merge_clusters(filename, migrant, ntol=0.02, rad_flag=True, lower=0, upper=10.0, rad_dict=None,clusterradii=0.5):
    with zopen(filename, "rt") as f:
        input_string = f.read()
    parser = CifParser_new.from_string(input_string)
    stru = parser.get_structures(primitive=False)[0]
    species = [str(sp).replace("Specie ", "") for sp in stru.species]
    elements = [re.sub('[^a-zA-Z]', '', sp) for sp in species]
    symm_number, symm_sybol = parser.get_symme()
    sitesym = parser.get_sym_opt()
    if migrant not in elements:
        raise ValueError("The input migrant ion not in the input structure! Please check it.")
    effec_radii, migrant_radius, migrant_alpha, nei_dises, coordination_list = LocalEnvirCom(stru, migrant)
    radii = {}
    if rad_flag:
        if rad_dict:
            radii = rad_dict
        else:
            radii = effec_radii
    atmnet = AtomNetwork.read_from_RemoveMigrantCif(filename, migrant, radii, rad_flag)
    vornet, edge_centers, fcs, faces = atmnet.perform_voronoi_decomposition(True, ntol)
    # add_fcs_vornet = vornet.add_facecenters(faces)
    prefixname = filename.replace(".cif", "")
    symprec = 0.01
    sym_vornet, voids = get_labeled_vornet(vornet, sitesym, symprec)
    writeNETFile(prefixname + "_origin.net", atmnet, sym_vornet)
    channels = Channel.findChannels2(vornet, atmnet, lower, upper, prefixname+".net")
    Channel.writeToVESTA(channels, atmnet, prefixname)
    conn_val = connection_values_list(prefixname + ".resex", sym_vornet)
    voids, channels = load_voids_channels_from_file(prefixname+".net")
    mc = MergeCluster(filename, stru, voids, channels,clusterradii = clusterradii)
    return conn_val

if __name__ == "__main__":
    # Calculate transport channel using given radii. 
    # The parameter clusterradii is used to control the distance of merged interstice clusters
    rad_dict = {"Li1": 0.73, "C1": 0.06, "O1": 1.24, "O2": 1.23}
    conn_val = cal_channel_merge_clusters("./cifs/Li/icsd_16713.cif","Li",ntol=0.02, rad_flag=True, lower=0.0, upper=10.0, rad_dict= rad_dict,clusterradii=0.5)
    print("conn_val:", conn_val)
    # Calculate transport channel using auto-calculate radii.
    cal_channel_merge_clusters("./cifs/Li/icsd_16713.cif","Li",ntol=0.02, rad_flag=True, lower=0.0, upper=10.0,clusterradii=0.5)


