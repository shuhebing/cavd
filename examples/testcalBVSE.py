# -*- coding: UTF-8 -*-

from  cavd import bv_calculation




if __name__ == "__main__":
    """
    calculate BVSE landscape
    :param filename: cif filename
    :param moveion:  move ion
    :param valenceofmoveion: valence of move ion
    :param resolution: resolution for calculating BVSE landsacpe
    :return: migration energy barrier, Pgrid file for saving and visualization BVSE landscape
    """
    Ea = bv_calculation("./cifs/Li/icsd_16713.cif","Li", 1,0.1)
    print("Ea:", Ea)
   


