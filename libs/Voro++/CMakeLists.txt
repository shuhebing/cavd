cmake_minimum_required( VERSION 3.12 )
set( CMAKE_CXX_FLAGS "-std=c++11" )
project(voro++)
include_directories(${PROJECT_SOURCE_DIR}/src/)
set(LIBSRC ${PROJECT_SOURCE_DIR}/src/
${PROJECT_SOURCE_DIR}/src/cell.cc
${PROJECT_SOURCE_DIR}/src/common.cc
${PROJECT_SOURCE_DIR}/src/v_base.cc
${PROJECT_SOURCE_DIR}/src/container.cc
${PROJECT_SOURCE_DIR}/src/unitcell.cc
${PROJECT_SOURCE_DIR}/src/container_prd.cc
${PROJECT_SOURCE_DIR}/src/pre_container.cc
${PROJECT_SOURCE_DIR}/src/v_compute.cc
${PROJECT_SOURCE_DIR}/src/c_loops.cc
${PROJECT_SOURCE_DIR}/src/wall.cc
)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/lib)

set(LIB_NAME voro++)
add_library(${LIB_NAME} STATIC ${LIBSRC})

